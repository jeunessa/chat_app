﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatView : MonoBehaviour 
{
	[SerializeField] MessageItem messageItemPrefab;
	[SerializeField] GameObject messageGrid;

	[Header("Tray")]
	[SerializeField] InputField messageField;

	[SerializeField] InputField senderNameField;

	void Start()
	{
		senderNameField.text = "squishy";
	}

	public void OnSend()
	{
		if (messageField.text.Equals (string.Empty))
		{
			return;
		}

		AddNewMessage (senderNameField.text, messageField.text);
		messageField.text = string.Empty;
	}

	void AddNewMessage(string sender, string message)
	{
		MessageItem messageItem = Instantiate(messageItemPrefab);
		messageItem.transform.SetParent (messageGrid.transform, false);
		messageItem.SetMessageContents (sender, message);
	}
}
