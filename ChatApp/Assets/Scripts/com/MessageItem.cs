﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageItem : MonoBehaviour 
{
	[SerializeField] Text messageLabel;

	public void SetMessageContents(string senderName, string message)
	{
		messageLabel.text = string.Format("<b>{0}</b>: {1}", senderName, message);
	}
}
